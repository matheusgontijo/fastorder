<?php declare(strict_types=1);

namespace MatheusGontijo\FastOrder\Repository\Form\FastOrderFormValidation;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;

class ProductNumberValidationRepository
{
    public function __construct(private EntityRepository $productRepository)
    {
    }

    public function searchValidProductNumbers(array $productNumbers, Context $context): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('productNumber', $productNumbers));

        $searchResult = $this->productRepository->search($criteria, $context);

        $validProductNumbers = [];

        foreach ($searchResult as $product) {
            assert($product instanceof ProductEntity);

            $validProductNumbers[] = $product->getProductNumber();
        }

        return $validProductNumbers;
    }
}
