<?php declare(strict_types=1);

namespace MatheusGontijo\FastOrder\Service;

use MatheusGontijo\FastOrder\Form\FastOrderFormValidation;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItemFactoryRegistry;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class FastOrderAddProductsToCart
{
    public function __construct(
        private LineItemFactoryRegistry $lineItemFactoryRegistry,
        private CartService $cartService,
        private EntityRepository $productRepository,
    ) {
    }

    public function add(Cart $cart, RequestDataBag $data, SalesChannelContext $context): void
    {
        $productIdsByProductNumber = $this->getProductIdsByProductNumber($data, $context);

        for ($i = 0; $i < FastOrderFormValidation::MAX_PRODUCT_FIELDS; $i++) {
            $productNumber = $this->getProductNumber($i, $data);
            $qty = $this->getQty($i, $data);

            if ($productNumber === '' || $qty === '') {
                continue;
            }

            $qty = (int) $qty;

            $lineItem = $this->lineItemFactoryRegistry->create([
                'type' => LineItem::PRODUCT_LINE_ITEM_TYPE,
                'referencedId' => $productIdsByProductNumber[$productNumber],
                'quantity' => $qty,
            ], $context);

            // @TODO: check if the product already exists, otherwise just update the qty,
            // otherwise it will have duplicate lines in the cart for the same product,
            // because the lack of time I haven't implemented it yet
            $this->cartService->add($cart, $lineItem, $context);
        }
    }

    private function getProductIdsByProductNumber(RequestDataBag $data, SalesChannelContext $context): array
    {
        $productNumbers = $this->getAllProductNumbers($data);

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('productNumber', $productNumbers));

        $products = $this->productRepository->search($criteria, $context->getContext());

        $productIdsByProductNumber = [];
        foreach ($products as $product) {
            assert($product instanceof ProductEntity);
            $productIdsByProductNumber[$product->getProductNumber()] = $product->getId();
        }

        return $productIdsByProductNumber;
    }

    private function getAllProductNumbers(RequestDataBag $data)
    {
        $productNumbers = [];

        for ($i = 0; $i < FastOrderFormValidation::MAX_PRODUCT_FIELDS; $i++) {
            $productNumbers[] = $this->getProductNumber($i, $data);
        }

        return $productNumbers;
    }

    private function getProductNumber(int $i, RequestDataBag $data): string
    {
        $items = $data->get('items');
        assert($items instanceof RequestDataBag);

        $item = $items->get((string) $i);
        assert($item instanceof RequestDataBag);

        $productNumber = $item->get('productNumber', '');
        assert(is_string($productNumber));

        return trim($productNumber);
    }

    private function getQty(int $i, RequestDataBag $data): string
    {
        $items = $data->get('items');
        assert($items instanceof RequestDataBag);

        $item = $items->get((string) $i);
        assert($item instanceof RequestDataBag);

        $qty = $item->get('qty', '');
        assert(is_string($qty));

        return trim($qty);
    }
}
