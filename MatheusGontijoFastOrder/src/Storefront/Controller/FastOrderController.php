<?php declare(strict_types=1);

namespace MatheusGontijo\FastOrder\Storefront\Controller;

use MatheusGontijo\FastOrder\Form\FastOrderFormValidation;
use MatheusGontijo\FastOrder\Service\FastOrderAddProductsToCart;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\Framework\Validation\Exception\ConstraintViolationException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

#[Route(defaults: ['_routeScope' => ['storefront']])]
class FastOrderController extends StorefrontController
{
    #[Route(
        path: '/fast-order',
        name: 'frontend.matheusGontijo.fastOrder.index.page',
        methods: ['GET'],
    )]
    public function fastOrderPage(Request $request, RequestDataBag $data): Response
    {
        return $this->renderStorefront('@MatheusGontijoFastOrder/storefront/page/fast-order/index.html.twig', [
            'maxProductFields' => FastOrderFormValidation::MAX_PRODUCT_FIELDS - 1,
            'data' => $data,
            'formViolations' => $request->attributes->get('formViolations'),
        ]);
    }

    #[Route(
        path: '/fast-order',
        name: 'frontend.matheusGontijo.fastOrder.index.save',
        methods: ['POST'],
    )]
    public function fastOrderSave(
        SalesChannelContext $context,
        RequestDataBag $data,
        FastOrderFormValidation $fastOrderFormValidation,
        Cart $cart,
        FastOrderAddProductsToCart $fastOrderAddProductsToCart,
    ): Response {
        $formViolations = $fastOrderFormValidation->validate($data, $context);

        if ($formViolations->count() > 0) {
            $formViolationsException = new ConstraintViolationException($formViolations, $data->all());

            return $this->forwardToRoute('frontend.matheusGontijo.fastOrder.index.page', [
                'formViolations' => $formViolationsException,
            ]);
        }

        try {
            $fastOrderAddProductsToCart->add($cart, $data, $context);

            $this->addFlash(self::SUCCESS, $this->trans('matheusGontijo.fastOrder.index.addToCartSuccess'));
        } catch (Throwable $e) {
            $this->addFlash(self::DANGER, $this->trans('matheusGontijo.fastOrder.index.addToCartError'));

            return $this->forwardToRoute('frontend.matheusGontijo.fastOrder.index.page');
        }

        return $this->redirectToRoute('frontend.checkout.cart.page');
    }
}
