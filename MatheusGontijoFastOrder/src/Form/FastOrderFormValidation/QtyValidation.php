<?php declare(strict_types=1);

namespace MatheusGontijo\FastOrder\Form\FastOrderFormValidation;

use MatheusGontijo\FastOrder\Form\FastOrderFormValidation;
use MatheusGontijo\FastOrder\Repository\Form\FastOrderFormValidation\ProductNumberValidationRepository;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class QtyValidation
{
    public function __construct()
    {
    }

    public function validate(
        ConstraintViolationList $constraintViolationList,
        RequestDataBag $data,
        SalesChannelContext $context
    ): ConstraintViolationList {
        for ($i = 0; $i < FastOrderFormValidation::MAX_PRODUCT_FIELDS; $i++) {
            $productNumber = $this->getProductNumber($i, $data);
            $qty = $this->getQty($i, $data);

            if ($productNumber === '' && $qty === '') {
                continue;
            }

            if ($qty === '') {
                $constraintViolation = new ConstraintViolation(
                    'matheusGontijo.fastOrder.index.validation.qty.pleaseEnterQty',
                    null,
                    [],
                    null,
                    sprintf('/qty%s', $i),
                    $qty,
                );

                $constraintViolationList->add($constraintViolation);
                continue;
            }

            $result = preg_match('/^[0-9]+$/', $qty);
            assert(is_int($result));

            if ($result === 0) {
                $constraintViolation = new ConstraintViolation(
                    'matheusGontijo.fastOrder.index.validation.qty.pleaseEnterADigit',
                    null,
                    [],
                    null,
                    sprintf('/qty%s', $i),
                    $qty,
                );

                $constraintViolationList->add($constraintViolation);
                continue;
            }

            $qtyInt = intval($qty);

            if ($qtyInt <= 0) {
                $constraintViolation = new ConstraintViolation(
                    'matheusGontijo.fastOrder.index.validation.qty.pleaseEnterAPositiveDigit',
                    null,
                    [],
                    null,
                    sprintf('/qty%s', $i),
                    $qty,
                );

                $constraintViolationList->add($constraintViolation);
                continue;
            }
        }

        return $constraintViolationList;
    }

    private function getProductNumber(int $i, RequestDataBag $data): string
    {
        $items = $data->get('items');
        assert($items instanceof RequestDataBag);

        $item = $items->get((string) $i);
        assert($item instanceof RequestDataBag);

        $productNumber = $item->get('productNumber', '');
        assert(is_string($productNumber));

        return trim($productNumber);
    }

    private function getQty(int $i, RequestDataBag $data): string
    {
        $items = $data->get('items');
        assert($items instanceof RequestDataBag);

        $item = $items->get((string) $i);
        assert($item instanceof RequestDataBag);

        $qty = $item->get('qty', '');
        assert(is_string($qty));

        return trim($qty);
    }
}
