<?php declare(strict_types=1);

namespace MatheusGontijo\FastOrder\Form\FastOrderFormValidation;

use MatheusGontijo\FastOrder\Form\FastOrderFormValidation;
use MatheusGontijo\FastOrder\Repository\Form\FastOrderFormValidation\ProductNumberValidationRepository;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class ProductNumberValidation
{
    public function __construct(private ProductNumberValidationRepository $productNumberValidationRepository)
    {
    }

    public function validate(
        ConstraintViolationList $constraintViolationList,
        RequestDataBag $data,
        SalesChannelContext $context
    ): ConstraintViolationList {
        $validProductNumbers = $this->prepareValidProductNumbers($data, $context);

        for ($i = 0; $i < FastOrderFormValidation::MAX_PRODUCT_FIELDS; $i++) {
            $productNumber = $this->getProductNumber($i, $data);
            $qty = $this->getQty($i, $data);

            if ($productNumber === '' && $qty === '') {
                continue;
            }

            if ($productNumber === '') {
                $constraintViolation = new ConstraintViolation(
                    'matheusGontijo.fastOrder.index.validation.productNumber.pleaseEnterAProductNumber',
                    null,
                    [],
                    null,
                    sprintf('/productNumber%s', $i),
                    $qty,
                );

                $constraintViolationList->add($constraintViolation);
                continue;
            }

            if (!in_array($productNumber, $validProductNumbers, true)) {
                $constraintViolation = new ConstraintViolation(
                    'matheusGontijo.fastOrder.index.validation.productNumber.theProductNumberIsNotValid',
                    null,
                    [],
                    null,
                    sprintf('/productNumber%s', $i),
                    $productNumber,
                );

                $constraintViolationList->add($constraintViolation);
            }
        }

        return $constraintViolationList;
    }

    private function prepareValidProductNumbers(RequestDataBag $data, SalesChannelContext $context): array
    {
        $validProductNumbers = [];

        $productNumbers = $this->getAllProductNumbers($data);

        if ($productNumbers === []) {
            return $validProductNumbers;
        }

        return $this->productNumberValidationRepository->searchValidProductNumbers(
            $productNumbers,
            $context->getContext()
        );
    }

    private function getAllProductNumbers(RequestDataBag $data): array
    {
        $productNumbers = [];

        for ($i = 0; $i < FastOrderFormValidation::MAX_PRODUCT_FIELDS; $i++) {
            $productNumber = $this->getProductNumber($i, $data);

            if ($productNumber === '') {
                continue;
            }

            $productNumbers[] = $productNumber;
        }

        return $productNumbers;
    }

    private function getProductNumber(int $i, RequestDataBag $data): string
    {
        $items = $data->get('items');
        assert($items instanceof RequestDataBag);

        $item = $items->get((string) $i);
        assert($item instanceof RequestDataBag);

        $productNumber = $item->get('productNumber', '');
        assert(is_string($productNumber));

        return trim($productNumber);
    }

    private function getQty(int $i, RequestDataBag $data): string
    {
        $items = $data->get('items');
        assert($items instanceof RequestDataBag);

        $item = $items->get((string) $i);
        assert($item instanceof RequestDataBag);

        $qty = $item->get('qty', '');
        assert(is_string($qty));

        return trim($qty);
    }
}
