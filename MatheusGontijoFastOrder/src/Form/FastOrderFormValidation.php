<?php declare(strict_types=1);

namespace MatheusGontijo\FastOrder\Form;

use MatheusGontijo\FastOrder\Form\FastOrderFormValidation\ProductNumberValidation;
use MatheusGontijo\FastOrder\Form\FastOrderFormValidation\QtyValidation;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\Validator\ConstraintViolationList;

class FastOrderFormValidation
{
    public const MAX_PRODUCT_FIELDS = 10;

    public function __construct(
        private ProductNumberValidation $productNumberValidation,
        private QtyValidation $qtyValidation,
    ) {
    }

    public function validate(RequestDataBag $data, SalesChannelContext $context): ConstraintViolationList
    {
        $constraintViolations = new ConstraintViolationList();

        $constraintViolations = $this->productNumberValidation->validate($constraintViolations, $data, $context);
        $constraintViolations = $this->qtyValidation->validate($constraintViolations, $data, $context);

        return $constraintViolations;
    }
}
